import { View, Button, ScrollView, Linking, Text } from 'react-native';
import React, { useMemo, useState, useEffect, ReactNode } from 'react';
import { ApproxDirections, GradientColors } from '../utils/constants';
import { Prediction } from '../components/Prediction';
import { getTransitions } from '../utils/transitions';
import { formatCloudLayers } from '../utils/clouds';
import { CelsiusCode, Coordinate, GetStationObservationsResponse, Observation, StationObservation, Transition, TransitionEventValues } from '../api/models';
import PageContainer from './PageContainer';
import { getAndSetPosition } from '../utils/geolocation';
import { StationScanListEntry, calculateCloudMovementDirection, celsiusToFahrenheit, retrieveStationObservations } from '../utils/stations';


// 2 weeks worth
const MaxTransitions = 2 * 7;
const HoursToReportLast = 1;
const MainPage = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [currentPosition, setCurrentPosition] = useState<Coordinate | undefined>(undefined);
    const [stations, setStations] = useState<StationScanListEntry | undefined>(undefined);
    const [stationObservations, setStationObservations] = useState<Record<string, GetStationObservationsResponse>>({});

    const nextTransitions = useMemo(() => {
        const now = new Date();
        const transitions: Transition[] = [];
        if (!currentPosition) return [];

        // collect the next N transitions
        do {
            const nowTransitions = getTransitions(currentPosition, now);

            const cutoff = new Date();
            cutoff.setHours(now.getHours() - HoursToReportLast);

            TransitionEventValues.forEach(value => {
                if (transitions.length >= MaxTransitions)
                    return;
                if (cutoff.getTime() < nowTransitions[value].getTime()) {
                    transitions.push({
                        event: value,
                        time: nowTransitions[value],
                        zenith: nowTransitions.zenith
                    });
                }
            });

            now.setDate(now.getDate() + 1);
        } while (transitions.length < MaxTransitions);


        return transitions;
    }, [currentPosition]);



    useEffect(() => {
        getAndSetPosition(setCurrentPosition);
    }, []);

    const collectObservations = async () => {
        if (!currentPosition || isLoading) return;

        setIsLoading(true);
        const { stations, observations } = await retrieveStationObservations(currentPosition);
        const filteredObservations = observations.reduce((accum, cur) => {
            if (cur.data) {
                accum[cur.id] = cur.data;
            }
            return accum;
        }, {} as Record<string, GetStationObservationsResponse>);

        setStations(stations);
        setStationObservations(filteredObservations);
        setIsLoading(false);
    }

    useEffect(() => { collectObservations() }, [currentPosition]);

    const stationData = useMemo(
        () => {
            if (!stations) {
                return {}
            }

            return Object.keys(stations).reduce((accum, cur) => {
                const station = stations[cur];
                const id = station.id;
                const [long, lat] = station.geometry.coordinates;
                if (!accum[id]) {
                    accum[id] = {
                        id,
                        coords: { lat, long },
                        observations: []
                    }
                }
                return accum;
            }, {} as Record<string, StationObservation>);
        }, [stations]);

    const observations: Record<string, StationObservation> = useMemo(
        () => {
            const compiled: Record<string, StationObservation> = {};
            Object.keys(stationObservations).forEach(key => {
                const obss = stationObservations[key];
                obss.features.forEach(obs => {
                    const station = obs.properties.station;
                    const timeStamp = Date.parse(obs.properties.timestamp);

                    if (!compiled[station]) {
                        compiled[station] = {
                            id: station,
                            coords: { lat: obs.geometry.coordinates[1], long: obs.geometry.coordinates[0] },
                            observations: []
                        };
                    }

                    const temperatureF = obs.properties.temperature.unitCode === CelsiusCode
                        ? celsiusToFahrenheit(obs.properties.temperature.value)
                        : obs.properties.temperature.value;
                    const cloudLayers = formatCloudLayers(obs.properties.cloudLayers);
                    const o: Observation = {
                        timeStamp,
                        temperatureF,
                        elevation: obs.properties.elevation.value,
                        seaLevelPressure: obs.properties.seaLevelPressure.value,
                        windSpeed: obs.properties.windSpeed.value || 1,
                        cloudLayers
                    };
                    compiled[station].observations!.push(o);
                })
            });


            return compiled;

        }, [stationData, stationObservations]);

    const center = useMemo(() => {
        var extremes = Object.values(observations).reduce((accum, cur) => {
            const coords = cur.coords;
            if (coords.lat < accum.min.lat) {
                accum.min.lat = coords.lat;
            }
            if (coords.lat > accum.max.lat) {
                accum.max.lat = coords.lat;
            }

            if (coords.long < accum.min.long) {
                accum.min.long = coords.long;
            }
            if (coords.long > accum.max.long) {
                accum.max.long = coords.long;
            }
            return accum;
        }, { max: { lat: -181, long: -181 }, min: { lat: 181, long: 181 } });

        return {
            lat: (extremes.min.lat + extremes.max.lat) / 2,
            long: (extremes.min.long + extremes.max.long) / 2
        }
    }, [observations])

    const cloudMovementDirection = useMemo(() =>
        calculateCloudMovementDirection(observations),
        [stationObservations]);

    const predictionElements: ReactNode[] = useMemo(() => {
        if (!currentPosition || isLoading) {
            return [
                <Text
                    key='loading'
                    style={{
                        verticalAlign: 'middle',
                        textAlign: "center",
                        fontSize: 42,
                        lineHeight: 48,
                        color: '#ccc',
                        textTransform: 'capitalize'
                    }}>
                    {isLoading ? "Loading..." : "BeautifulSun needs location permissions."}
                </Text>
            ];

        }
        const refreshButton = (
            <Button
                key="refresh"
                title="Refresh"
                onPress={() => getAndSetPosition(setCurrentPosition)}
                color={GradientColors[0]}
            />
        );
        const codeButton = (
            <View key="view-code" style={{ marginTop: 24, }}>
                <Button
                    key="view-code"
                    title="View code"
                    onPress={() => Linking.openURL('https://gitlab.com/lexi-drake/beautifulsun')}
                    color={GradientColors[0]}
                />
            </View>
        );

        const elements = nextTransitions.map((transition: Transition) =>
            <Prediction
                key={`${transition.event}_${transition.time.getTime()}`}
                title={transition.event}
                direction={ApproxDirections[transition.event]}
                timeOfTransition={transition.time}
                stationObservations={observations}
                currentPosition={center}
                cloudMovementDirection={cloudMovementDirection}
                zenith={transition.zenith[transition.event]}
            />
        );

        return [
            elements.slice(0, 2),
            refreshButton,
            ...elements.slice(2),
            codeButton
        ]
    }, [stationObservations, nextTransitions, getAndSetPosition, isLoading, currentPosition]);

    return (
        <PageContainer>
            <ScrollView
                bounces={false}
                showsVerticalScrollIndicator={false}
            >
                {predictionElements}
            </ScrollView>
        </PageContainer>
    );
};

export default MainPage;

