import { ReactNode } from "react";
import { Platform, Text, View } from 'react-native';
import { LinearGradient } from "react-native-linear-gradient";
import { GradientColors } from "../utils/constants";

const PageContainer = ({ children }: { children: ReactNode }) =>
    <View style={{
        height: '100%'
    }}>
        <LinearGradient style={{
            flex: 1,
            height: '100%',
            width: '100%',
            alignItems: 'center',
            marginTop: Platform.OS === 'ios' ? '5%' : 0
        }} colors={GradientColors}>
            <View style={{
                flex: 1,
                width: '100%'
            }}>
                {children}
            </View>
        </LinearGradient>
    </View>
export default PageContainer;