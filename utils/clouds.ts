import { CloudLayerInfo, CloudLayerResponse, CloudLevel, CloudMeasureValue, FullCloudMeasureStrings, TransitionEvent } from "../api/models";

export const CloudLayerMax = 8;
export const CloudCeilingMeters: Record<CloudLevel, number> = {
    low: 2000,
    middle: 6000,
    high: 13000
};

const CloudLevelMovementFactor: Record<CloudLevel, number> = Object.keys(CloudCeilingMeters)
    .reduce((accum: Record<CloudLevel, number>, cur: string) => {
        const cl = cur as CloudLevel;
        accum[cl] = Math.sqrt(CloudCeilingMeters[cl] / CloudCeilingMeters.low);
        return accum;
    }, {} as Record<CloudLevel, number>);

// This accounts for the different movement speeds of clouds at different heights.
export const applyCloudLevelMovementFactor = (level: CloudLevel, { x, y }: { x: number, y: number }) => ({
    x: CloudLevelMovementFactor[level] * x,
    y: CloudLevelMovementFactor[level] * y
});

export const CloudMeasureOrder = ['CLR', 'FEW', 'SCT', 'BKN', 'OVC'] as const;
const CloudLayerMins: Record<CloudMeasureValue, number> = {
    OVC: 6,
    BKN: 4,
    SCT: 2,
    FEW: 0.1,
    SKC: 0,
    CLR: 0,
    VV: 8
}
const CloudLayerMaxs: Record<CloudMeasureValue, number> = {
    OVC: CloudLayerMax,
    BKN: 6,
    SCT: 4,
    FEW: 2,
    SKC: 0.1,
    CLR: 0.1,
    VV: 8
}
export const CloudMeasures: Record<CloudMeasureValue, number> = FullCloudMeasureStrings.reduce((accum, cur) => {
    accum[cur] = (CloudLayerMins[cur] + CloudLayerMaxs[cur]) / 2;
    return accum;
}, {} as Record<CloudMeasureValue, number>);

export type CloudMeasureString = typeof CloudMeasureOrder[number];

// React Native cannot handle template-strings or variables as arguments to `require`.
export const CloudLayerImages: Record<string, Record<string, any>> = {
    low: {
        ovc: require('../assets/ovc_low.png'),
        bkn: require('../assets/bkn_low.png'),
        few: require('../assets/few_low.png'),
        sct: require('../assets/sct_low.png'),
        clr: require('../assets/clr.png')
    },
    middle: {
        ovc: require('../assets/ovc_low.png'),
        bkn: require('../assets/bkn_low.png'),
        few: require('../assets/few_low.png'),
        sct: require('../assets/sct_low.png'),
        clr: require('../assets/clr.png')
    },
    high: {
        ovc: require('../assets/ovc_high.png'),
        bkn: require('../assets/bkn_high.png'),
        few: require('../assets/few_high.png'),
        sct: require('../assets/sct_high.png'),
        clr: require('../assets/clr.png')
    }
} as const;

export const formatCloudLayers = (cloudLayers: CloudLayerResponse[]): Partial<CloudLayerInfo>[] => {
    return cloudLayers.map((layer) => {
        const meters = layer.base.value;
        const amount = CloudMeasures[layer.amount];

        if (!meters || meters < CloudCeilingMeters.low) {
            return { low: amount }
        } else if (meters < CloudCeilingMeters.middle) {
            return { middle: amount }
        }
        return { high: amount }
    });
}

export const getCloudMeasureString = (avg: number): CloudMeasureString =>
    CloudMeasureOrder.filter(key =>
        (CloudLayerMins[key] <= avg && CloudLayerMaxs[key] >= avg)
    )[0] || "CLR";

const getHeaviness = (value: number) =>
    value > CloudLayerMax * (3 / 4)
        ? ''
        : value > CloudLayerMax / 2
            ? 'moderate '
            : 'scattered ';

const createHighCloudDescription = (cloudLayers: CloudLayerInfo): string => {
    if (cloudLayers.high > 7) {
        return 'colorful high-altitude clouds'
    }
    const heaviness = getHeaviness(cloudLayers.high);

    if (cloudLayers.high > CloudLayerMaxs.CLR) {
        return `${heaviness}colorful high-altitude clouds`
    }

    return 'clear skies';
}


const createMiddleCloudDescription = (cloudLayers: CloudLayerInfo): string => {
    if (cloudLayers.middle > 7) {
        return 'middle-layer clouds'
    }

    const highDescription = createHighCloudDescription(cloudLayers);
    const modifier = cloudLayers.middle < CloudLayerMax / 2 ? 'possible ' : '';
    const above = cloudLayers.low > CloudLayerMaxs.CLR ? '' : ' above'
    const optionalCloudDescription = cloudLayers.high > CloudLayerMaxs.CLR
        ? ` with ${modifier}${highDescription}${above}`
        : '';

    const heaviness = getHeaviness(cloudLayers.middle);
    if (cloudLayers.middle > CloudLayerMaxs.CLR) {
        return `${heaviness} middle-altitude clouds${optionalCloudDescription}`
    }

    return highDescription;
}

const createLowMiddleCloudDescription = (cloudLayers: CloudLayerInfo): string => {
    const lowAndMiddle = cloudLayers.low + cloudLayers.middle;
    if (lowAndMiddle > 7) {
        return 'low-altitude clouds'
    }

    const highDescription = createHighCloudDescription(cloudLayers);
    if (lowAndMiddle > CloudLayerMax * (3 / 4)) {
        return `low-altitude clouds with the possibility of ${highDescription} peeking through`;
    }

    const heaviness = getHeaviness(lowAndMiddle);
    if (lowAndMiddle > CloudLayerMaxs.CLR) {
        return `${heaviness}low-altitude clouds obscuring the ${highDescription} above`;
    }
    return highDescription;
}

export const getCloudCoverDescription = (cloudLayers: CloudLayerInfo) => {
    const total = Object.keys(cloudLayers).reduce((accum, key) => {
        accum += cloudLayers[key as keyof typeof cloudLayers];
        return accum;
    }, 0);

    if (total < CloudLayerMaxs.CLR * 3) {
        return 'clear skies';
    }

    return createLowMiddleCloudDescription(cloudLayers)
}