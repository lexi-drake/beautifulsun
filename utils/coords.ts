import { Coordinate } from "../api/models";
import { Direction } from "./constants";

const EarthRadiusKM = 6371
const EarthRadiusMeters = EarthRadiusKM * 1000;
export const degreesToRadians = (degrees: number) => degrees * Math.PI / 180;
export const radiansToDegrees = (radians: number) => radians * (180 / Math.PI);
export const KMPerLatDegree = (Math.PI * EarthRadiusKM * 2) / 360;

// adapted from https://community.esri.com/t5/coordinate-reference-systems-blog/distance-on-a-sphere-the-haversine-formula/ba-p/902128
// and https://en.wikipedia.org/wiki/Haversine_formula#Formulation
export const getKMAndAngleBetweenCoordinates = (from: Coordinate | undefined, to: Coordinate) => {
    if (!from) {
        return { kilometers: 0, angle: 0 };
    }

    const fromLatRadians = degreesToRadians(from.lat);
    const toLatRadians = degreesToRadians(to.lat);
    const deltaLatRadians = degreesToRadians(to.lat - from.lat);
    const deltaLongRadians = degreesToRadians(to.long - from.long);

    // d = 2r * arcsin(sqrt(a))
    const a = Math.sin(deltaLatRadians / 2) ** 2 + Math.cos(fromLatRadians) * Math.cos(toLatRadians) * Math.sin(deltaLongRadians / 2) ** 2;
    const kilometers = 2 * EarthRadiusKM * Math.asin(Math.sqrt(a));
    const normal = getNormalVectorBetweenCoords(from, to);

    return {
        kilometers,
        angle: Math.atan(normal.y / normal.x)
    };
}

export type NormalCoord = {
    x: number;
    y: number;
}

export const getNormalVectorBetweenCoords = (from: Coordinate, to: Coordinate): NormalCoord => {
    const deltaLat = to.lat - from.lat;
    const deltaLong = to.long - from.long;

    const y = deltaLat * Math.PI / 180 * EarthRadiusMeters;
    const latFactor = Math.cos(degreesToRadians(deltaLat / 2));
    const x = deltaLong * Math.PI / 180 * EarthRadiusMeters * latFactor;

    const l = Math.sqrt(x * x + y * y);

    const norm = {
        x: x / l,
        y: y / l
    };

    return norm;
}

export const getCoordinatesAfterTravel = (start: Coordinate, travel: NormalCoord): Coordinate => {
    const newLat = start.lat + radiansToDegrees((travel.y) / EarthRadiusMeters);
    const avgLat = (newLat + start.lat) / 2;
    const latFactor = Math.cos(degreesToRadians(avgLat));
    const radiansOfTravel = travel.x / EarthRadiusMeters;

    const newCoords = {
        lat: newLat,
        long: start.long + radiansToDegrees(radiansOfTravel) / latFactor
    };

    return newCoords;
}

export const getDirectionToCoords = (from: Coordinate, to: Coordinate): Direction =>
    to.long > from.long ? 'east' : 'west';
