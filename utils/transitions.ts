import { degreesToRadians, radiansToDegrees } from "./coords";
import { Coordinate } from "../api/models";

const SunOrigin = Date.parse('1899-12-30');
const MoonOrigin = Date.parse('2000-01-01');
const MsPerHour = 1000 * 60 * 60;
const MsPerDay = MsPerHour * 24;

// ods formats numbers as the number of days since ~1900
const daysSinceSunOrigin = (date: Date) => (date.getTime() - SunOrigin) / MsPerDay;
const daysSinceMoonOrigin = (date: Date) => (date.getTime() - MoonOrigin) / MsPerDay;
export const msToHours = (ms: number) => ms / MsPerHour;

// Adapted from spreadsheets found at https://gml.noaa.gov/grad/solcalc/calcdetails.html
export const getTransitions = (coords: Coordinate, date: Date) => {
    const latRadians = degreesToRadians(coords.lat);

    const today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    const timezoneOffset = date.getTimezoneOffset() / -60;
    const days = daysSinceSunOrigin(today);

    const timePastMidnight = 0.1 / 24;

    const julienDay = days + 2415018.5 + timePastMidnight - timezoneOffset / 24;
    const julienCentury = (julienDay - 2451545) / 36525;
    const geomMeanLongSunDegrees = (280.46646 + julienCentury * (36000.76983 + julienCentury * 0.0003032)) % 360;
    const geomMeanLongSunRadians = degreesToRadians(geomMeanLongSunDegrees);
    const geomMeanAnomSunDegrees = 357.52911 + julienCentury * (35999.05029 - 0.0001537 * julienCentury);
    const geomMeanAnomSunRadians = degreesToRadians(geomMeanAnomSunDegrees);
    const eccentEarthOrbit = 0.016708634 - julienCentury * (0.000042037 + 0.0000001267 * julienCentury);
    const sunEqOfCtr =
        Math.sin(geomMeanAnomSunRadians) *
        (1.914602 - julienCentury * (0.004817 + 0.000014 * julienCentury)) +
        Math.sin(2 * geomMeanAnomSunRadians) *
        (0.019993 - 0.000101 * julienCentury) +
        Math.sin(3 * geomMeanAnomSunRadians) * 0.000289;
    const sunTrueLong = geomMeanLongSunDegrees + sunEqOfCtr;
    const sunAppLong = sunTrueLong - 0.00569 - 0.00478 * Math.sin(degreesToRadians(125.04 - 1934.136 * julienCentury));
    const meanObliqEcliptic = 23 + (26 + ((21.448 - julienCentury * (46.815 + julienCentury * (0.00059 - julienCentury * 0.001813)))) / 60) / 60;
    const oblicCorr = meanObliqEcliptic + 0.00256 * Math.cos(degreesToRadians(125.04 - 1934.136 * julienCentury));

    const sunDeclinRadians = Math.asin(
        Math.sin(degreesToRadians(oblicCorr)) *
        Math.sin(degreesToRadians(sunAppLong))
    );

    const y = Math.tan(degreesToRadians(oblicCorr / 2)) ** 2;
    const eqOfTime = 4 * radiansToDegrees(
        y * Math.sin(2 * geomMeanLongSunRadians) -
        2 * eccentEarthOrbit * Math.sin(geomMeanAnomSunRadians) +
        4 * eccentEarthOrbit * y * Math.sin(geomMeanAnomSunRadians) * Math.cos(2 * geomMeanLongSunRadians) -
        0.5 * y * y * Math.sin(4 * geomMeanLongSunRadians) -
        1.25 * eccentEarthOrbit * eccentEarthOrbit * Math.sin(2 * geomMeanAnomSunRadians)
    );

    const haSunrise = radiansToDegrees(
        Math.acos(
            Math.cos(degreesToRadians(90.833)) / (Math.cos(latRadians) * Math.cos(sunDeclinRadians)) -
            Math.tan(latRadians) * Math.tan(sunDeclinRadians)
        )
    );

    const solarNoon = (720 - 4 * coords.long - eqOfTime + timezoneOffset * 60) / 1440;
    const sunrise = solarNoon - haSunrise * 4 / 1440;
    const sunset = solarNoon + haSunrise * 4 / 1440;

    const sunriseSolarZenithAngleRadians = Math.acos(Math.sin(latRadians) * Math.sin(sunDeclinRadians) + Math.cos(latRadians) * Math.cos(sunDeclinRadians) * Math.cos(degreesToRadians(sunrise)))
    const sunseteSolarZenithAngleRadians = Math.acos(Math.sin(latRadians) * Math.sin(sunDeclinRadians) + Math.cos(latRadians) * Math.cos(sunDeclinRadians) * Math.cos(degreesToRadians(sunset)))
    // The spreadsheet calculates sunrise & sunset as fractions of 24 hours, so they need to be converted back into ms
    const sunriseDateTime = new Date(today.getTime() + (sunrise * MsPerDay));
    const sunsetDateTime = new Date(today.getTime() + (sunset * MsPerDay));

    return {
        sunrise: sunriseDateTime,
        sunset: sunsetDateTime,
        zenith: {
            sunrise: sunriseSolarZenithAngleRadians,
            sunset: sunseteSolarZenithAngleRadians
        },
        timezoneOffset,
    };
}

// Note: I was exploring some moon things. I'm leaving this here but will
// probably not return to it. Maybe. We'll see.
export const getMoonDeclination = (coords: Coordinate, date: Date) => {
    const days = daysSinceMoonOrigin(date);
    const twoPi = Math.PI * 2;
    const u = days - 0.48 * Math.sin((twoPi * (days - 3.45) / 27.55455));

    const tropicalDeltaDegrees = 23.4 * Math.sin((twoPi * (u - 10.75)) / 27.32158);
    const draconicDeltaDegrees = 5.1 * Math.sin((twoPi * (u - 20.15)) / 27.21222);

    const declination = tropicalDeltaDegrees + draconicDeltaDegrees;

    const t = Math.atan(Math.tan(coords.lat) * Math.tan(declination));
}