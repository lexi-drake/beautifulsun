import { TransitionEvent } from "../api/models";

export const GradientColors = ['#141d47', '#7f95be', '#7fb0be', '#edbe75'];

export type Direction = 'east' | 'west';

export const ApproxDirections: Record<TransitionEvent, Direction> = {
    sunrise: 'east',
    sunset: 'west'
};