import { getPoint, getStationObservations, getStations } from "../api/api";
import { Coordinate, GetPointResponse, StationFeatures, StationObservation } from "../api/models";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CloudCeilingMeters } from "./clouds";
import { getNormalVectorBetweenCoords } from "./coords";

const StationScanListKey = 'STATIONS_SCAN_LIST';
export type StationScanListEntry = Record<string, StationFeatures>
type StationScanList = Record<string, StationScanListEntry>

const getStationScanList = async (point: GetPointResponse, gridKey: string): Promise<StationScanListEntry> => {
    const json = await AsyncStorage.getItem(StationScanListKey);
    const scanList: StationScanList = json ? JSON.parse(json) : {};

    if (scanList[gridKey]) return scanList[gridKey];

    const uniqueStations: Record<string, StationFeatures> = {}
    const stations = await getStations(point.properties.observationStations)

    stations.features.forEach(features => {
        if (!uniqueStations[features.properties.stationIdentifier]) {
            uniqueStations[features.properties.stationIdentifier] = features;
        }
    });

    const promises = Object.keys(uniqueStations).map(key => {
        const features = uniqueStations[key];
        return getStationObservations(features.properties.stationIdentifier, 4)
            .then(data => ({
                id: features.properties.stationIdentifier,
                name: features.properties.name,
                data
            }))
            .catch(() => ({
                id: features.properties.stationIdentifier,
                name: features.properties.name,
                data: undefined
            }))
    });
    const observations = await Promise.all(promises);
    const stationsToKeep: StationScanListEntry = observations.reduce((accum, cur) => {
        if (cur.data) {
            for (const feature of cur.data.features) {
                if (feature.properties.cloudLayers.length)
                    return {
                        ...accum,
                        [cur.id]: uniqueStations[cur.id]
                    };
            }
        }
        return accum;
    }, {});

    await setStationScanListList(gridKey, stationsToKeep);
    return stationsToKeep;
}

const setStationScanListList = async (gridId: string, entry: StationScanListEntry) => {
    const json = await AsyncStorage.getItem(StationScanListKey);

    const ignoreList: StationScanList = json ? JSON.parse(json) : {};
    ignoreList[gridId] = entry;

    await AsyncStorage.setItem(StationScanListKey, JSON.stringify(ignoreList))
}

export const celsiusToFahrenheit = (c: number) => ((9 / 5) * c) + 32;

export const retrieveStationObservations = async (coords: Coordinate, limit = 24) => {
    const point = await getPoint(coords);
    const { gridId } = point.properties;

    const scanList = await getStationScanList(point, gridId);
    const promises = Object.keys(scanList)
        .map(id => getStationObservations(id, limit)
            .then(data => ({
                id,
                name: scanList[id],
                data
            }))
            .catch(() => ({
                id,
                name: scanList[id],
                data: undefined
            }))
        );

    const observations = await Promise.all(promises);
    return {
        point,
        stations: scanList,
        observations
    };
}

const ConstPressureSpecificHeat = 1004.68506;
const SeaLevelStandardTemp = 288.16;
const GravitationalAcceleration = 9.80665;
const DryAirMolMass = 0.02896968;
const UniversalGasConstant = 8.314462618;

// Adapted from https://en.wikipedia.org/wiki/Atmospheric_pressure
const getPressureAtElevation = (stationElevation: number, seaLevelPressure: number) => {
    const height = stationElevation + CloudCeilingMeters.middle;

    const exponent = (ConstPressureSpecificHeat * DryAirMolMass) / UniversalGasConstant;
    const numerator = GravitationalAcceleration * height;
    const denominator = ConstPressureSpecificHeat * SeaLevelStandardTemp;
    const pressureAtElevation = seaLevelPressure * (1 - (numerator / denominator)) ** exponent;
    return pressureAtElevation;
}

type PressureMap = {
    coords: Coordinate;
    value: number;
}[];

export const createPressureMap = (stationObservations: Record<string, StationObservation>) => Object.keys(stationObservations).reduce((accum, key) => {
    const station = stationObservations[key];
    for (const obs of station.observations) {
        if (obs.seaLevelPressure)
            accum.push({
                coords: station.coords,
                value: getPressureAtElevation(obs.elevation, obs.seaLevelPressure)
            });
    }
    return accum;
}, [] as PressureMap);

// Clouds don't move according to the ground-level wind (which is reported by api.weather.gov).
// Cloud movement is, however, affected by pressure gradients. This uses a pressure-map to determine the
// local pressure-gradient. The "vector" of the pressure gradient (low to high) is determined
// using statistical regression.
// Adapted from http://faculty.cas.usf.edu/mbrannick/regression/Part3/Reg2.html
export const calculateCloudMovementDirection = (stationObservations: Record<string, StationObservation>) => {
    const results: Record<string, { sum: number, mean: number, dev: number, uss: number, values: number[] }> = {
        y: {
            sum: 0,
            mean: 0,
            dev: 0,
            uss: 0,
            values: []
        },
        x1: {
            sum: 0,
            mean: 0,
            dev: 0,
            uss: 0,
            values: []
        },
        x2: {
            sum: 0,
            mean: 0,
            dev: 0,
            uss: 0,
            values: []
        },
        x1y: {
            sum: 0,
            mean: 0,
            dev: 0,
            uss: 0,
            values: []
        },
        x2y: {
            sum: 0,
            mean: 0,
            dev: 0,
            uss: 0,
            values: []
        },
        x1x2: {
            sum: 0,
            mean: 0,
            dev: 0,
            uss: 0,
            values: []
        },
    };

    const pressureMap = createPressureMap(stationObservations);
    pressureMap.forEach(({ coords, value }) => {
        const xOne = coords.long;
        const xTwo = coords.lat;
        const y = value;

        results.x1.sum += xOne;
        results.x1.values.push(xOne);
        results.x2.sum += xTwo;
        results.x2.values.push(xTwo);
        results.y.sum += y;
        results.y.values.push(y);

        const x1y = xOne * y;
        const x2y = xTwo * y;
        const x1x2 = xOne * xTwo;

        results.x1y.sum += x1y;
        results.x1y.values.push(x1y);
        results.x2y.sum += x2y;
        results.x2y.values.push(x2y);
        results.x1x2.sum += x1x2;
        results.x1x2.values.push(x1x2);
    });


    const n = pressureMap.length;
    Object.keys(results).forEach(key => {
        const values = results[key];
        const mean = values.sum / n;
        const devSum = values.values.map(v => (v - mean) ** 2)
            .reduce((sum, v) => sum + v, 0);
        const dev = Math.sqrt(devSum / (n - 1));

        values.mean = mean;
        values.dev = dev;
        values.uss = devSum;
    });

    const x1y = results.x1y.sum - (results.x1.sum * results.y.sum / n);
    const x2y = results.x2y.sum - (results.x2.sum * results.y.sum / n);
    const x1x2 = results.x1x2.sum - (results.x1.sum * results.x2.sum / n);

    const denominator = ((results.x1.uss * results.x2.uss) - x1x2 ** 2);
    const b1 = ((results.x2.uss * x1y) - (x1x2 * x2y)) / denominator;
    const b2 = ((results.x1.uss * x2y) - (x1x2 * x1y)) / denominator;
    const intercept = results.y.mean - (b1 * results.x1.mean) - (b2 * results.x2.mean);

    const regression = (x1: number, x2: number): number => {
        const long = b1 * x1;
        const lat = b2 * x2;
        return intercept + long + lat;
    }

    const yPrimes = pressureMap.map(v => {
        const yPrime = regression(v.coords.long, v.coords.lat);
        return yPrime;
    });

    const yPrimeSum = yPrimes.reduce((sum, cur) => sum + cur, 0);
    const yPrimeMean = yPrimeSum / n;
    const yPrimeVariance = yPrimes.map(v => (v - yPrimeMean) ** 2)
        .reduce((sum, v) => sum + v, 0) / (n - 1);

    const variance = results.y.dev ** 2;
    const rSquare = yPrimeVariance / variance;
    // TODO: these might be useful when adding the "explain this prediction" feature
    // const k = 2;
    // const f = (rSquare / k) / ((1 - rSquare) / (n - k - 1));

    // using long and lat as approximations of x & y 
    // we have a vector that describes the pressure gradient established by
    // our station observations.
    const gradientVector = { x: b1, y: b2 };

    // rotate the vector 90 degrees clockwise to account for the 
    // corealis effect (assuming user is in northern hemisphere because 
    // this is using US government data).
    const movement = {
        long: gradientVector.y,
        lat: -gradientVector.x
    };

    return {
        rSquare,
        normal: getNormalVectorBetweenCoords({ lat: 0, long: 0 }, movement)
    };
}