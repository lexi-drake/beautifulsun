import { Platform, PermissionsAndroid } from "react-native";
import GeoLocation from 'react-native-geolocation-service';
import { Coordinate } from "../api/models";

export const getAndSetPosition = async (setPosition: (coord?: Coordinate) => void) => {
    if (Platform.OS === 'ios') {
        const auth = await GeoLocation.requestAuthorization("whenInUse");
        if (auth !== "granted") return;
    }

    if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                title: 'BeautifulSun',
                message: 'BeautifulSun needs to access your location.',
                buttonPositive: 'Allow'
            }
        );
        if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
            return;
        }
    }
    GeoLocation.getCurrentPosition(
        position => {
            setPosition({
                lat: position.coords.latitude,
                long: position.coords.longitude
            })
        },
        () => {
            setPosition(undefined);
        });
};