import React, { useMemo } from "react";
import { View, Text, Image } from "react-native"
import { CloudLayerImages, applyCloudLevelMovementFactor, getCloudCoverDescription, getCloudMeasureString } from "../utils/clouds";
import { getKMAndAngleBetweenCoordinates, getCoordinatesAfterTravel, NormalCoord, getDirectionToCoords } from "../utils/coords";
import { msToHours } from "../utils/transitions";
import { DateTime } from 'luxon';
import LinearGradient from 'react-native-linear-gradient';
import { CloudLevel, Coordinate, StationObservation, TransitionEvent } from "../api/models";
import { Direction, GradientColors } from "../utils/constants";

type PredictionProps = {
    title: TransitionEvent;
    direction: Direction;
    timeOfTransition: Date;
    stationObservations: Record<string, StationObservation>;
    currentPosition: Coordinate;
    zenith: number;
    cloudMovementDirection: { rSquare: number, normal: NormalCoord };
};

type CondensedStationData = {
    id: string;
    origCoords: Coordinate;
    coords: Coordinate;
    kilometers: number;
    angle: number;
    cloudLayers: Partial<Record<CloudLevel, number>>;
    timeStamp: number;
    hoursUntilTransition: number;
};

const MaxKM = 150;
export const Prediction = ({
    title,
    direction,
    timeOfTransition,
    stationObservations,
    currentPosition,
    cloudMovementDirection,
    zenith
}: PredictionProps) => {

    const relevantStationData: CondensedStationData[] = useMemo(
        () => Object.keys(stationObservations)
            .map(key => {
                const station = stationObservations[key]
                if (!station.observations.length) return [];

                const observations: CondensedStationData[] = [];
                station.observations.forEach(observation => {
                    const { cloudLayers, timeStamp, windSpeed } = observation;
                    const hoursUntilTransition = msToHours(timeOfTransition.getTime() - timeStamp);

                    // This is a temporary approximation that, I think, under-estimates the amount of travel
                    // to a handful of km per hour. A quick internet search suggests that average cloud
                    // speed is much greater than that (30-120MPH; https://www.coolkidfacts.com/how-fast-do-clouds-move/)

                    // Cloud speed is affected by the pressure gradient, such that the stronger the effect 
                    // causes faster movement.
                    const pressureFactor = 1 / (1 - cloudMovementDirection.rSquare ** 2);

                    // Ground wind-speed is also considered as a factor in determining the speed of the clouds
                    const applyFactors = (v: number) => v * 5 * hoursUntilTransition * 1000 * pressureFactor * Math.sqrt(windSpeed);
                    const baseMetersToTravel = {
                        x: applyFactors(cloudMovementDirection.normal.x),
                        y: applyFactors(cloudMovementDirection.normal.y)
                    };

                    const cloudObservations = cloudLayers.map(info => {
                        const key = Object.keys(info)[0] as CloudLevel;
                        const metersToTravel = applyCloudLevelMovementFactor(key, baseMetersToTravel);
                        const coords = getCoordinatesAfterTravel(station.coords, metersToTravel);
                        const { kilometers, angle } = getKMAndAngleBetweenCoordinates(currentPosition, coords);

                        return {
                            level: key,
                            value: info[key],
                            coords,
                            kilometers,
                            angle
                        }
                    });

                    cloudObservations.forEach(({ coords, kilometers, angle, level, value }) =>
                        observations.push({
                            id: station.id,
                            origCoords: station.coords,
                            coords,
                            kilometers,
                            angle,
                            cloudLayers: { [level]: value },
                            hoursUntilTransition,
                            timeStamp,
                        }));
                });
                return observations;
            })
            .flat(1)
            .filter(data => {
                if (!data || !data.id)
                    return false;

                // really basic direction-check
                const d = getDirectionToCoords(currentPosition, data.coords);
                return d === direction && data.kilometers < MaxKM;
            }),
        [stationObservations, direction]);

    const cloudLayerAggregates = useMemo(() => {
        const cloudLayers = {
            low: {
                weightedSum: 0,
                weightedAvg: 0,
                description: ""
            },
            middle: {
                weightedSum: 0,
                weightedAvg: 0,
                description: "",
            },
            high: {
                weightedSum: 0,
                weightedAvg: 0,
                description: ""
            }
        };
        let totalWeight = 0;
        var centerLocation = title == 'sunrise' ? zenith : -zenith;
        relevantStationData.forEach(data => {
            const weight = Math.abs((1 / ((data.angle - centerLocation) ** 2) || 0.001) * data.hoursUntilTransition);
            totalWeight += weight;

            const low = data.cloudLayers.low || 0;
            const middle = data.cloudLayers.middle || 0;
            const high = data.cloudLayers.high || 0;

            cloudLayers.low.weightedSum += (low * weight);
            cloudLayers.middle.weightedSum += (middle * weight);
            cloudLayers.high.weightedSum += (high * weight);

        });

        if (totalWeight > 0)
            Object.keys(cloudLayers).forEach((layer: string) => {
                const layerKey = layer as keyof typeof cloudLayers;
                const weightedAvg = cloudLayers[layerKey].weightedSum === 0 ? 0 : (cloudLayers[layerKey].weightedSum / totalWeight);
                cloudLayers[layerKey].weightedAvg = weightedAvg;
                cloudLayers[layerKey].description = getCloudMeasureString(weightedAvg)
            });
        return cloudLayers;
    }, [relevantStationData]);

    const dateString = useMemo(
        () => {
            const dt = DateTime.fromISO(timeOfTransition.toISOString());
            const date = dt.toLocaleString(DateTime.DATE_MED);
            const time = dt.toLocaleString(DateTime.TIME_SIMPLE);
            return `${date} at ${time}`;
        },
        [timeOfTransition]
    );

    const prediction = useMemo(() => `Expect ${getCloudCoverDescription({
        low: cloudLayerAggregates.low.weightedAvg,
        middle: cloudLayerAggregates.middle.weightedAvg,
        high: cloudLayerAggregates.high.weightedAvg
    })}`,
        [title, relevantStationData, cloudLayerAggregates]);


    const images = useMemo(() => {
        const imageOffsets = { low: 72, middle: 36, high: 0 };
        return Object.keys(cloudLayerAggregates).reduce((accum, cur) => {
            const measure = cloudLayerAggregates[cur as keyof typeof cloudLayerAggregates].description.toLowerCase();
            const source = CloudLayerImages[cur][measure];
            accum[cur] = <Image
                source={source}
                style={{
                    position: 'absolute',
                    alignSelf: 'center',
                    transform: [cur == 'middle' ? { scale: 1 } : { scaleX: -1 }],
                    top: imageOffsets[cur as keyof typeof imageOffsets]
                }}
                resizeMode="stretch"
            />
            return accum;
        }, {} as any);
    }, [cloudLayerAggregates]);
    if (!relevantStationData.length) {
        return (<></>);
    }

    return (
        <LinearGradient
            colors={GradientColors}
            style={{
                width: '96%',
                borderRadius: 10,
                margin: '2%',
                padding: '2%',
                paddingBottom: '4%',
                flex: 1,
                height: 300
            }}
        >
            <View style={{
                flex: 2,
            }}>
                <Text style={{
                    textAlign: "center",
                    fontSize: 24,
                    lineHeight: 32,
                    color: '#ccc',
                    textTransform: 'capitalize'
                }}>{title.toString()}</Text>
                <Text style={{
                    textAlign: "center",
                    color: '#ccc'
                }}>{dateString}</Text>
                <Text style={{
                    textAlign: "left",
                    color: '#ccc',
                    margin: 8
                }}>{prediction}</Text>
            </View>
            <View style={{
                flex: 4,
            }}>
                {images.high}
                {images.middle}
                {images.low}
            </View>
        </LinearGradient>
    );
}
