export type Coordinate = {
    lat: number;
    long: number;
};

export type GetStationsResponse = {
    features: StationFeatures[];
};

export type StationFeatures = {
    id: string;
    geometry: {
        coordinates: number[];
    }
    properties: {
        stationIdentifier: string;
        name: string;
    }
};

export const CelsiusCode = 'wmoUnit:degC';
export type StationObservationResponse = {
    id: string;
    geometry: {
        coordinates: number[];
    },
    properties: {
        elevation: {
            value: number;
        }
        station: string;
        timestamp: string;
        temperature: {
            unitCode: string;
            value: number;
        }
        windSpeed: {
            value: number;
        }
        seaLevelPressure: {
            value?: number;
        }
        cloudLayers: CloudLayerResponse[];
    }
}
export type GetStationObservationsResponse = {
    features: StationObservationResponse[];
};

export const FullCloudMeasureStrings = ['VV', 'CLR', 'SKC', 'FEW', 'SCT', 'BKN', 'OVC'] as const;
export type CloudMeasureValue = typeof FullCloudMeasureStrings[number];
export type CloudLayerResponse = {
    base: { value?: number },
    amount: CloudMeasureValue
};

export type GetPointResponse = {
    properties: {
        observationStations: string;
        gridId: string;
        gridX: number;
        gridY: number;
    }
};

export type StationObservation = {
    id: string,
    observations: Observation[],
    coords: Coordinate,
};

export type CloudLayerInfo = {
    low: number,
    middle: number,
    high: number
};

export type CloudLevel = keyof CloudLayerInfo;

export type Observation = {
    timeStamp: number;
    windSpeed: number;
    elevation: number;
    temperatureF: number;
    seaLevelPressure?: number;
    cloudLayers: Partial<CloudLayerInfo>[];
};

export const TransitionEventValues = ['sunrise', 'sunset'] as const;
export type TransitionEvent = typeof TransitionEventValues[number];
export type Transition = {
    event: TransitionEvent;
    time: Date;
    zenith: { sunrise: number, sunset: number };
};

