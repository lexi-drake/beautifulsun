import axios from 'axios';
import { Coordinate, GetPointResponse, GetStationObservationsResponse, GetStationsResponse } from './models';

const client = axios.create({
    baseURL: 'https://api.weather.gov',
    headers: {
        'user-agent': 'OregonSunrise; alexa@alexadrake.rocks',
        'accept': 'application/geo+json'
    },
});

export const getPoint = async (coord: Coordinate): Promise<GetPointResponse> =>
    (await client.get(`points/${coord.lat},${coord.long}`)).data;

export const getStations = async (stationslink: string): Promise<GetStationsResponse> =>
    (await axios.get(stationslink).then(data => data)
        .catch(() => {
            return { data: { features: [] } };
        }
        )).data;

export const getStationObservations = async (id: string, limit = 4): Promise<GetStationObservationsResponse> =>
    (await client.get(`/stations/${id}/observations?limit=${limit}`)).data;
