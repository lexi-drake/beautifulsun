import { it, expect } from '@jest/globals';
import { StationObservation } from '../../api/models';
import { calculateCloudMovementDirection, createPressureMap } from '../../utils/stations';

it('should create correct pressure map', () => {
    const stationObservations: Record<string, StationObservation> = {
        a: {
            id: 'a',
            observations: [{
                elevation: 20,
                seaLevelPressure: 1,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            },
            {
                elevation: 10,
                seaLevelPressure: 1,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            }],
            coords: { lat: 0, long: 0 }
        },
        b: {
            id: 'b',
            observations: [{
                elevation: 20,
                seaLevelPressure: 10,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            }],
            coords: { lat: 1, long: 0 }
        }
    };

    const pressureMap = createPressureMap(stationObservations);
    expect(pressureMap.length).toBe(3);
    expect(pressureMap[2].value).toBeGreaterThan(pressureMap[0].value);
    expect(pressureMap[2].value).toBeGreaterThan(pressureMap[1].value);
    expect(pressureMap[1].value).toBeGreaterThan(pressureMap[0].value);
});

it('should calculate east movement direction near equator', () => {
    const stationObservations: Record<string, StationObservation> = {
        a: {
            id: 'a',
            observations: [{
                elevation: 20,
                seaLevelPressure: 1,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            }],
            coords: { lat: 0, long: 1 }
        },
        b: {
            id: 'b',
            observations: [{
                elevation: 20,
                seaLevelPressure: 1,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            }],
            coords: { lat: 0, long: -1 }
        },
        c: {
            id: 'c',
            observations: [{
                elevation: 20,
                seaLevelPressure: 30,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            }],
            coords: { lat: 2, long: 0 }
        }
    };

    const movementDirection = calculateCloudMovementDirection(stationObservations);
    expect(movementDirection.normal.x).toBeCloseTo(1);
    expect(movementDirection.normal.y).toBeCloseTo(0);
});

it('should calculate east movement direction away from equator', () => {
    const stationObservations: Record<string, StationObservation> = {
        a: {
            id: 'a',
            observations: [{
                elevation: 20,
                seaLevelPressure: 1,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            }],
            coords: { lat: 60, long: -61 }
        },
        b: {
            id: 'b',
            observations: [{
                elevation: 20,
                seaLevelPressure: 1,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            }],
            coords: { lat: 60, long: -59 }
        },
        c: {
            id: 'c',
            observations: [{
                elevation: 20,
                seaLevelPressure: 30,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            }],
            coords: { lat: 62, long: -60 }
        }
    };

    const movementDirection = calculateCloudMovementDirection(stationObservations);
    expect(movementDirection.normal.x).toBeCloseTo(1);
    expect(movementDirection.normal.y).toBeCloseTo(0);
});

it('should calculate south movement direction away from equator', () => {
    const stationObservations: Record<string, StationObservation> = {
        a: {
            id: 'a',
            observations: [{
                elevation: 20,
                seaLevelPressure: 1,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            }],
            coords: { lat: 59, long: -60 }
        },
        b: {
            id: 'b',
            observations: [{
                elevation: 20,
                seaLevelPressure: 1,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            }],
            coords: { lat: 61, long: -60 }
        },
        c: {
            id: 'c',
            observations: [{
                elevation: 20,
                seaLevelPressure: 30,
                temperatureF: 32,
                timeStamp: 0,
                windSpeed: 0,
                cloudLayers: []
            }],
            coords: { lat: 60, long: -59 }
        }
    };

    const movementDirection = calculateCloudMovementDirection(stationObservations);
    expect(movementDirection.normal.x).toBeCloseTo(0);
    expect(movementDirection.normal.y).toBeCloseTo(-1);
});