import { it, expect } from '@jest/globals';
import { CloudLayerInfo, CloudLayerResponse } from '../../api/models';
import { CloudMeasures, formatCloudLayers, getCloudCoverDescription, getCloudMeasureString } from '../../utils/clouds';

it('should format cloud layers correctly', () => {
    const cloudResponses: CloudLayerResponse[] = [
        {
            base: { value: 1000 },
            amount: 'BKN'
        }, {
            base: { value: 2500 },
            amount: 'FEW'
        }, {
            base: { value: 8000 },
            amount: 'OVC'
        }
    ];

    const cloudLayers = formatCloudLayers(cloudResponses);
    expect(cloudLayers.length).toBe(3);
    expect(cloudLayers[0].low).toBe(CloudMeasures.BKN);
    expect(cloudLayers[1].middle).toBe(CloudMeasures.FEW);
    expect(cloudLayers[2].high).toBe(CloudMeasures.OVC);
});

it('should get ovc measure string', () => {
    const measure = 6.2;
    const str = getCloudMeasureString(measure);

    expect(str).toBe('OVC');
});

it('should get bkn measure string', () => {
    const measure = 4.2;
    const str = getCloudMeasureString(measure);

    expect(str).toBe('BKN');
});

it('should get sct measure string', () => {
    const measure = 2.2;
    const str = getCloudMeasureString(measure);

    expect(str).toBe('SCT');
});

it('should get few measure string', () => {
    const measure = 1.2;
    const str = getCloudMeasureString(measure);

    expect(str).toBe('FEW');
});

it('should get clr measure string', () => {
    const measure = 0.02;
    const str = getCloudMeasureString(measure);

    expect(str).toBe('CLR');
});

it('should report clear skies', () => {
    const cloudLayers: CloudLayerInfo = {
        low: 0,
        middle: 0,
        high: 0
    };

    const description = getCloudCoverDescription(cloudLayers);
    expect(description).toBe('clear skies');
});

it('should report low altitude clouds', () => {
    const cloudLayers: CloudLayerInfo = {
        low: 7.1,
        middle: 0,
        high: 0
    };

    const description = getCloudCoverDescription(cloudLayers);
    expect(description).toBe('low-altitude clouds');
});

it('should report low and middle', () => {
    const cloudLayers: CloudLayerInfo = {
        low: 6.1,
        middle: 7.1,
        high: 0
    };

    const description = getCloudCoverDescription(cloudLayers);
    expect(description).toBe('low-altitude clouds');
});

it('should report moderate low and middle', () => {
    const cloudLayers: CloudLayerInfo = {
        low: 6,
        middle: 7.1,
        high: 0
    };

    const description = getCloudCoverDescription(cloudLayers);
    expect(description).toBe('low-altitude clouds');
});

it('should report scattered low and high', () => {
    const cloudLayers: CloudLayerInfo = {
        low: 3,
        middle: 0,
        high: 7.1
    };

    const description = getCloudCoverDescription(cloudLayers);
    expect(description).toBe('scattered low-altitude clouds obscuring the colorful high-altitude clouds above');
});

it('should report scattered middle and high', () => {
    const cloudLayers: CloudLayerInfo = {
        low: 0,
        middle: 4,
        high: 7
    };

    const description = getCloudCoverDescription(cloudLayers);
    expect(description).toBe('scattered low-altitude clouds obscuring the colorful high-altitude clouds above');
});

it('should report scattered middle and high', () => {
    const cloudLayers: CloudLayerInfo = {
        low: 0,
        middle: 3.9,
        high: 6
    };

    const description = getCloudCoverDescription(cloudLayers);
    expect(description).toBe('scattered low-altitude clouds obscuring the moderate colorful high-altitude clouds above');
});

it('combine low and middle', () => {
    const cloudLayers: CloudLayerInfo = {
        low: 6,
        middle: 3.9,
        high: 6
    };

    const description = getCloudCoverDescription(cloudLayers);
    expect(description).toBe('low-altitude clouds');
});