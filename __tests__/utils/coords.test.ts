import { it, expect } from '@jest/globals';
import { Coordinate } from '../../api/models';
import { KMPerLatDegree, getCoordinatesAfterTravel, getDirectionToCoords, getKMAndAngleBetweenCoordinates, getNormalVectorBetweenCoords, radiansToDegrees } from '../../utils/coords';

// magic numbers and rounding to validate using this official tool https://www.nhc.noaa.gov/gccalc.shtml
const ZeroCoord: Coordinate = { lat: 0, long: 0 };

it('returns 0s if undefined from', () => {
    const to: Coordinate = { lat: 1, long: 1 };
    const { kilometers } = getKMAndAngleBetweenCoordinates(undefined, to);
    expect(kilometers).toBe(0);
})

it('gets km & angle for lat-only at equator', () => {
    const to: Coordinate = { lat: 1, long: 0 };

    const { kilometers } = getKMAndAngleBetweenCoordinates(ZeroCoord, to);
    expect(kilometers).toBeCloseTo(KMPerLatDegree, 1);
});

it('gets km & angle for long-only at equator', () => {
    const to: Coordinate = { lat: 0, long: 1 };

    const { kilometers } = getKMAndAngleBetweenCoordinates(ZeroCoord, to);
    expect(kilometers).toBeCloseTo(KMPerLatDegree, 1);
});

it('gets km & angle for lat & long at equator', () => {
    const to: Coordinate = { lat: 1, long: 1 };

    const { kilometers } = getKMAndAngleBetweenCoordinates(ZeroCoord, to);

    expect(Math.round(kilometers)).toBeCloseTo(157);
});

it('gets km & angle for lat only away from equator', () => {
    const from: Coordinate = { lat: 60, long: -60 };
    const to: Coordinate = { lat: 61, long: -60 };

    const { kilometers } = getKMAndAngleBetweenCoordinates(from, to);
    expect(kilometers).toBeCloseTo(KMPerLatDegree);
});

it('gets km & angle for long only away from equator', () => {
    const from: Coordinate = { lat: 60, long: -60 };
    const to: Coordinate = { lat: 60, long: -61 };
    const { kilometers } = getKMAndAngleBetweenCoordinates(from, to);

    expect(Math.round(kilometers)).toBeCloseTo(56);
});

it('gets km & angle for lat & long away from equator', () => {
    const from: Coordinate = { lat: 60, long: -60 };
    const to: Coordinate = { lat: 61, long: -61 };

    const { kilometers } = getKMAndAngleBetweenCoordinates(from, to);

    expect(Math.round(kilometers)).toBeCloseTo(124);
});

it('converts coord to normal with only lat', () => {
    const to: Coordinate = { lat: 10, long: 0 };
    const normal = getNormalVectorBetweenCoords(ZeroCoord, to);

    // these can be -0
    expect(normal.x).toBeCloseTo(0);
    expect(normal.y).toBe(1);
});

it('converts coord to normal with only long', () => {
    const to: Coordinate = { lat: 0, long: 10 };
    const normal = getNormalVectorBetweenCoords(ZeroCoord, to);

    expect(normal.x).toBe(1);
    expect(normal.y).toBeCloseTo(0);
});

it('converts coord to normal with lat & long', () => {
    const to: Coordinate = { lat: 1, long: 1 };
    const normal = getNormalVectorBetweenCoords(ZeroCoord, to);

    const normalAngle = Math.atan(normal.y / normal.x);
    expect(normalAngle).toBeCloseTo(Math.PI / 4);
});

it('gets correct travel coordinates near equator', () => {
    const start: Coordinate = { lat: 0, long: 0 };
    const travel = {
        y: KMPerLatDegree * 1000,
        x: KMPerLatDegree * 1000
    };

    const coordinatesAfterTravel = getCoordinatesAfterTravel(start, travel);
    expect(coordinatesAfterTravel.lat).toBeCloseTo(1);
    // because km/longitude is < km/latitude off of the equator, expect the
    // delta long to be > delta lat
    expect(coordinatesAfterTravel.long).toBeGreaterThan(1);
    expect(coordinatesAfterTravel.long).toBeCloseTo(1);
});

it('gets correct travel coordinates away from equator', () => {
    const start: Coordinate = { lat: 60, long: -60 };
    const travel = {
        y: KMPerLatDegree * 1000,
        x: KMPerLatDegree * 1000
    };

    const coordinatesAfterTravel = getCoordinatesAfterTravel(start, travel);
    expect(coordinatesAfterTravel.lat).toBeCloseTo(61);
    // -60 -> -58 is 108km at 60deg lat, so 111km is just off of -58
    expect(Math.round(coordinatesAfterTravel.long)).toBe(-58);
});

it('correctly identifies west', () => {
    const from: Coordinate = { lat: 60, long: -60 };
    const to: Coordinate = { lat: 60, long: -61 };

    const direction = getDirectionToCoords(from, to);
    expect(direction).toBe('west');
});

it('correctly identifies east', () => {
    const from: Coordinate = { lat: 60, long: -60 };
    const to: Coordinate = { lat: 60, long: -59 };

    const direction = getDirectionToCoords(from, to);
    expect(direction).toBe('east');
})
