import React from 'react';
import MainPage from './pages/MainPage';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const App = () => {

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='home' screenOptions={{
        header: () => null
      }}>
        <Stack.Screen
          name='home'
          component={MainPage}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;