 # BeautifulSun

A friend said "I wish there was an app that would tell me if the sunset (or sunrise) is going to be beautiful" and I figured, why not use this an an opportunity to learn React Native.

## Design

First things first: I want to make this app for free. I don't want to pay for API-access or data of any sort; therefore the data is pulled from `https://api.weather.gov`.

I'm not a meteorologist, and I don't intend to predict weather. 

This is based on the idea that high cloud layers are those that make sun-transitions more colorful, whereas lower cloud layers obscure the view of the sun-transition. 